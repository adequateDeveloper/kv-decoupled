defmodule KV do
  @moduledoc """
  KV public API.

  ## Examples
      # Note: start interactive session from the project root folder with: $ iex -S mix
      iex> KV.get "kv_test"
      nil
      iex> KV.put "kv_test", 3
      :ok
      iex> KV.get "kv_test"
      3
      iex> KV.delete "kv_test"
      3
      iex> KV.get "kv_test"
      nil
  """

  alias KV.Bucket

  @doc """
  Get the value for `key`.
  """
  defdelegate get(key), to: Bucket

  @doc """
  Put the `value` for the given `key`.
  """
  defdelegate put(key, value), to: Bucket

  @doc """
  Delete the `key`.

  Returns the current value of `key`, if `key` exists.
  """
  defdelegate delete(key), to: Bucket
end
