defmodule KV.Bucket do
  @moduledoc """
  Documentation for Bucket.
  """

  use Agent

  @doc """
  Start a new bucket named `MyBucket`.
  """
  def start_link do
    Agent.start_link(fn -> %{} end, name: MyBucket)
  end

  @doc """
  Put the `value` for the given `key` in `MyBucket`.
  """
  def put(key, value) do
    Agent.update(MyBucket, &Map.put(&1, key, value))
  end

  @doc """
  Get the value for `key` from `MyBucket`.
  """
  def get(key) do
    Agent.get(MyBucket, &Map.get(&1, key))
  end

  @doc """
  Delete `key` from `MyBucket`.

  Returns the current value of `key`, if `key` exists.
  """
  def delete(key) do
    Agent.get_and_update(MyBucket, &Map.pop(&1, key))
  end
end
