defmodule KV.Application do
  @moduledoc false

  use Application

  alias KV.Bucket

  def start(_type, _args) do
    children = [
      %{id: Bucket, start: {Bucket, :start_link, []}}
    ]

    opts = [
      strategy: :one_for_one,
      name: KV.Supervisor
    ]

    Supervisor.start_link(children, opts)
  end
end
