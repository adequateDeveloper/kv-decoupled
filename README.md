[![pipeline status](https://gitlab.com/adequateDeveloper/kv-decoupled/badges/master/pipeline.svg)](https://gitlab.com/adequateDeveloper/kv-decoupled/commits/master)


# Decoupled KV

An application of a [Dave Thomas (PragDave) blog article](https://pragdave.me/blog/2017/07/13/decoupling-interface-and-implementation-in-elixir.html) about decoupling interface and implementation in Elixir to the [Elixir Agent](https://elixir-lang.org/getting-started/mix-otp/agent.html) tutorial.


## Installation and setup

Download from the repo, and from the project root folder run:

```elixir
$ mix deps.get
```

Generate the project documentation by running from the project root folder:

```elixir
$ mix docs
```
and view them at "doc/index.html".

To get an overview of the tests, run from the project root folder:

```elixir
$ mix test --trace
```