defmodule KVTest do
  use ExUnit.Case, async: true

  # interfers with tests
  # doctest KV

  test "get/1 and put/2" do
    assert nil == KV.get("kv_test")

    assert :ok == KV.put("kv_test", 3)
    assert 3 == KV.get("kv_test")
  end

  test "delete/1" do
    assert nil == KV.get("kv_delete_test")
    assert :ok == KV.put("kv_delete_test", 3)
    assert 3 == KV.get("kv_delete_test")

    assert 3 == KV.delete("kv_delete_test")
    assert nil == KV.get("kv_delete_test")
  end
end
