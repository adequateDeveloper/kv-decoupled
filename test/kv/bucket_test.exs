defmodule KV.BucketTest do
  use ExUnit.Case, async: true
  alias KV.Bucket
  #  doctest Bucket

  test "get/1 and put/2" do
    assert nil == Bucket.get("bucket_test")

    assert :ok == Bucket.put("bucket_test", 3)
    assert 3 == Bucket.get("bucket_test")
  end

  test "delete/1" do
    assert nil == Bucket.get("delete_test")
    assert :ok == Bucket.put("delete_test", 3)
    assert 3 == Bucket.get("delete_test")

    assert 3 == Bucket.delete("delete_test")
    assert nil == Bucket.get("delete_test")
  end
end
